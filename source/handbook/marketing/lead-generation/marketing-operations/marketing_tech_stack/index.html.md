---
layout: markdown_page
title: "Marketing OPS Tech Stack Contracts &amp; Tool Details"
---

## On This Page
- [Contract Details](#contracts)
- [Tool How-Tos & FAQs](#howto)

## Contract Details <a name="contracts"></a>  
The contracts can only be viewed if you are a GitLab Team Member and signed into your account.  

### Clearbit  
[Executed contract copy](https://drive.google.com/open?id=0BzllC63GKDQHMTk2VnE5eWx3NHFTNjBwdHdmWXNCbG10d3pn)    
Effective Date: 22 March 2017   
End Date: 21 March 2018     
**GitLab Admins**: JJ Cordz or Francis Aquino   


### FunnelCake  
[Executed Contract copy](https://drive.google.com/open?id=0BzllC63GKDQHWjVlR0R5UVFjWnpvckZnTlFLUzJvTXZ0blpn)    
Effective Date: 31 August 2016   
End Date: 30 August 2017   
**GitLab Admin**: JJ Cordz  


### InsideView  
[Executed Contract copy](https://drive.google.com/open?id=0BzllC63GKDQHRWJLaFhiV3VOVmlUOXBLQmhmTnJrcjVRMVo4)    
Effective Date: 1 October 2016  
End Date: 30 September 2017 (12-month term)  
Cancellation Clause: 30-day notice to non-renew _Notice of non-renew given on 22 May 2017_  
**GitLab Admin**: JJ Cordz  


### Lean Data  
[Executed Contract copy](https://drive.google.com/open?id=0BzllC63GKDQHQTRYdjZGYk00TDRZMXZYTzJrSXNmNF9BM0xv)    
Effective Date: 1 May 2017  
End Date: 30 April 2018  
**GitLab Admins**: JJ Cordz or Francis Aquino  


### Marketo  
[Executed Contract copy](https://drive.google.com/open?id=0BzllC63GKDQHMWltTlRDdVg1UGs)   
Effective Date: 1 October 2016  
End Date: 30 September 2017  
**GitLab Admins**: JJ Cordz or Mitchell Wright    


### Olark  
Month to Month commitment    
**GitLab Admin**: Mitchell Wright   


### Outreach  
[Executed Contract copy](https://drive.google.com/open?id=0BzllC63GKDQHY2Y3MWs2Q082OUJTeDNaeUJ0bllyVXRWMjhN)  
Effective Date: 18 October 2016  
End Date: 17 October 2017   
**GitLab Admins**: JJ Cordz, Chet Backman or Francis Aquino (Sales OPS Admin)    


### Unbounce  
Month to Month commitment  
**GitLab Admin**: Mitchell Wright  


## Tool How-tos &amp; FAQ<a name="howto"></a>
- [Clearbit](#clearbit)
- [DiscoverOrg](#discoverorg)
- [FunnelCake](#funnelcake)
- [Google Analytics](#ga)
- [Google Tag Manager](#gtm)
- [InsideView](#insideview)
- [LeanData](#leandata)
- [Marketo](#marketo)
- [Olark](#olark)
- [Outreach.io](#outreachio)
- Piwik
- [Salesforce](#salesforce)
- [Unbounce](#unbounce)
- [Zoom](#zoom)   


### Clearbit<a name="clearbit"></a>
Clearbit is a data enrichment tool that will be used to fill in important information for new leads created in Salesforce. This information could include address, phone number, industry, IT employees and other important values when assessing both the quality of the lead as well as routing. The technology uses proprietary logic to match prospects to companies in their extensive database.  

#### Frequently Asked Questions

1. **What standard fields does Clearbit write to?** Clearbit will write to any standard field that is either blank or has a value of '[[unknown]]' if there is data to populate.

    **Lead**  
    First Name  
    Last Name (if value is [[unknown]])  
    Company (if value is [[unknown]])  
    Title  
    Phone (Company Line only)  
    Website  
    Address  
    No. of Employees  
    Industry  
    Description  
    Annual Revenue (for public companies)  

    **Contact**  
    First Name  
    Last Name (if value is [[unknown]])  
    Title  
    Mailing Address  
    Phone  
    Description  

    **Account**  
    Account Name (if value is [[unknown]])  
    Billing Address  
    Phone  
    Industry  
    Employees  
    Description  
    Annual Revenue (for public companies)  

2. **What fields does Clearbit use to identify people and companies?** The integration looks at the _email_ and _website_ fields on **leads** and **contacts**, and the _website_ field on **accounts**.

3. **Does Clearbit data update automatically?** YES - Clearbit data is automatically researched whenever a lead/contact/account is created, even if the record is not manaully opeened by a user. This happens through a Salesforce background job. The lead will also be triggred when the lead is viewed.   

### DiscoverOrg<a name="discoverorg"></a>  
DiscoverOrg provides our Sales Development Representatives and Account Executives with access to hundreds of thousands of prospects and their contact information, company infomation, tech stack, revenue, and other relevant data. Individual records or bulk exports can imported into Salesforce using extensive search criteria such as job function, title, industry, location, tech stack, employee count, and company revenue.  


### FunnelCake<a name="funnelcake"></a>   
FunnelCake helps manage performance to revenue across the customer lifecycle – from new lead generation to expansion opportunities - calculating core metrics like conversion rates, deal velocity, rep carry capacity, etc. FunnelCake works backwards from your revenue targets to identify gaps in your funnel early enough to solve them. Evaluate your lead generation plan, your pipeline health, individual opportunity health, and overall goals. FunnelCake aligns everything around time, helping you focus on the right strategies based on when you'll see impact from your efforts.


### Google Analytics<a name="ga"></a>  
Google Analytics captures all the analytics for GitLab pages outside of the actual app on https://gitlab.com. This tool is managed by the [Online Marketing Manager](https://about.gitlab.com/jobs/online-marketing-manager/) and detailed use information can be found in [Online Marketing section](https://about.gitlab.com/handbook/marketing/lead-generation/online-marketing/) of the Handbook.


### Google Tag Manager<a name="gtm"></a>  
Google Tag Manager is used to manage the firing of the different JavaScript tags we deploy on our website. All marketing tags should be deployed through Google Tag Manager except in cases such as A/B testing software. This tool is managed by the [Online Marketing Manager](https://about.gitlab.com/jobs/online-marketing-manager/) and detailed use information can be found in [Online Marketing section](https://about.gitlab.com/handbook/marketing/lead-generation/online-marketing/) of the Handbook.


### InsideView <a name="insideview"></a> 
InsideView is a lead enrichment tool. When a lead is created in Marketo, before synced to Salesforce, a InsideView webhook would be called to enrich specific fields of data.  
Will be discontinued at end of contract


### Lean Data<a name="leandata"></a>
When a lead is created in Salesforce, LeanData will be the tool that routes it to the appropriate user. Routing rules include sales segmentation, region, lead source, and owned accounts. For example, if a lead from a named account is created, it will be routed directly to the owner of the named account. Also, LeanData provide cross-object visibility between leads and accounts and contacts. When in an account record, a user can view "matched" leads by company name, email domain, and other criteria.  
[Operation Information about Lean Data](https://about.gitlab.com/handbook/sales/sales_ops/leandata/)


### Marketo<a name="marketo"></a>
Marketo is our marketing automation platform managed by our Marketing OPS team. Anyone who signs up for a trial, requests contact from GitLab, attends a webinar or trade show, or engages in any other marketing activity, will end up in Marketo. These prospects will receive scores based on their level of engagement and these scores will be used by the Business Developement Representatives and Account Executives to prioritize which prospects to follow up with. Marketo is also our primary tool for delivering marketing communciation to prospects and customers. You can read more about the Marketo and the other tools in the Marketing stack here.  

Important Documentation Links
- [Marketo Webhooks](http://developers.marketo.com/webhooks/)


#### Marketo Training Videos  
Here are links to the recordings of Marketo trainings that we've done: 
- [Email Creation and Tokens](https://drive.google.com/open?id=0B1_ZzeTfG3XYZjUwa3ZFb2ZWeWc)
- [How to use Smart Lists and Reporting](https://drive.google.com/open?id=0B1_ZzeTfG3XYcGV2VUFiR0dNaWM)  


#### Marketo Tools Server  
This is a simple Sinatra application that receives several webhooks and forwards the relevant information to Marketo via its REST API.
- URL: http://marketo-tools.gitlab.com/ (can't access via browser)  
- [Software running on it](https://gitlab.com/gitlab-com/marketo-tools/) 
- [Readme of the cookbook for deployment](https://gitlab.com/gitlab-com/cookbook-marketo-tools/blob/master/README.md)
<br>

### Olark <a name="olark"></a>  
[Olark](https://www.olark.com/) is a live chat tool that is installed on the Contact Sales and EE Trial pages to engage with customers (existing and potential) in real-time to help answer questions, on-board and provide direct support. Chat transcripts are pushed into Salesforce (when appropriate).  


### Outreach.io<a name="outreachio"></a>
Outreach.io is a tool used by the Sales team (RD/AE/BDRs) to automate emails in the form of sequences. Users can track open rates, click through rates, response rates for various templates and update sequences based on these metrics. These email sequences and communications are synced back to the Activity History in Salesforce.   


### Piwik  


### Salesforce<a name="salesforce"></a>  
Salesforce is our CRM of record and the _one source of truth_ for customer data. It integrates with all of the other applications in our business &amp; marketing tech stacks. Salesforce stores prospect, customer, and partner information. This includes contact information, products purchased, bookings, support tickets, and invoices, among other information.


### Unbounce<a name="unbounce"></a>  
Unbounce is for the creation of landing pages - PPC traffic, webcasts, live events, trainings, gated content. The login information is in the marketing vault of 1Password. Currently, the only landing pages within Unbounce are for our PPC traffic.  
**Before** any Unbounce landing pages are published, the design team needs to review &amp; approve to ensure the page meets our brand standards.


### Zoom<a name="zoom"></a>
Zoom is the video conferencing software used by GitLab for company-wide meetings. In June 2017, we are trialing the web conferencing service (replacing On24) to host our webcasts. Zoom is managed by People OPS. Detailed information about how to set up a personal account on Zoom can be found in the [Tools and Tips](https://about.gitlab.com/handbook/tools-and-tips/#zoom) section of the handbook.  

